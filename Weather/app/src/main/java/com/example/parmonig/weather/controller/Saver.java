package com.example.parmonig.weather.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.parmonig.weather.model.Forecast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by parmonig on 10/28/17.
 */

public class Saver {

    public Saver(){}

    private static final String KEY = "FORECAST";
    private static final String KIEV_ID = "703448";


    public void writeForecastToSharedPref(List<String> codes, Context context){
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sPref.edit();
        List<Forecast> forecasts = new ArrayList<>();
        for(String s: codes){
            forecasts.add(new ForecastGetter().getForecast(s));
        }
        Gson gson = new Gson();
        String json = gson.toJson(forecasts);
        editor.putString(KEY, json);
        editor.apply();
    }

    public void writeForecastToSharedPrefOnLaunch(Context context){
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(new ForecastGetter().getForecast(KIEV_ID));
        editor.putString(KEY, json);
        editor.apply();
    }

    public List<Forecast> readForecastFromSharedPref(Context context){
        List<Forecast> forecasts = new ArrayList<>();
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = sPref.getString(KEY, "");
        Type type = new TypeToken<List<Forecast>>(){}.getType();
        forecasts = gson.fromJson(json, type);
        return forecasts;
    }


    public  void writeForecastToAppStorageOnLaunch(Context context){
        try(FileOutputStream fos = context.openFileOutput(KEY, Context.MODE_PRIVATE); ObjectOutputStream os = new ObjectOutputStream(fos)) {
            os.writeObject(new ForecastGetter().getForecast(KIEV_ID));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeForecastToAppStorage(List<String> codes, Context context){
        try(FileOutputStream fos = context.openFileOutput(KEY, Context.MODE_PRIVATE); ObjectOutputStream os = new ObjectOutputStream(fos)) {
            for(String s: codes)
                os.writeObject(new ForecastGetter().getForecast(s));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Forecast> readForecastFromAppStorage(Context context){
        List<Forecast> forecasts = new ArrayList<>();
        try(FileInputStream fis = context.openFileInput(KEY); ObjectInputStream is = new ObjectInputStream(fis)){
            while (true) {
                Forecast f = (Forecast) is.readObject();
                if (f != null)
                    forecasts.add(f);
                else
                    break;
            }
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return forecasts;
    }

}
