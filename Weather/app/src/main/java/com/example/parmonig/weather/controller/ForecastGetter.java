package com.example.parmonig.weather.controller;

import android.os.AsyncTask;

import com.example.parmonig.weather.model.Forecast;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by parmonig on 10/28/17.
 */

public class ForecastGetter {

    public ForecastGetter(){}

    public Forecast getForecast(String cityId){
        try {
            return new AsyncRequest().execute(cityId).get();//making request not in main thread
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static final String API_KEY = "&appid=a19e265bc948efaff00e80097b1db5b6";
    private static final String UNITS = "&units=metric";
    private static final String CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?";
    private static final String WEEK_FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";

    private JsonObject getWeatherJson(String weatherUrl, String cityID){
        JsonObject obj = null;
        String u = weatherUrl + "id=" + cityID + UNITS + API_KEY;

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(u).build();
        Response responses = null;

        try {
            responses = client.newCall(request).execute();
        }catch (IOException e){
            e.printStackTrace();
        }
        JsonElement root = null;
        try {
            root = new JsonParser().parse(responses.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
        obj = root.getAsJsonObject();
        return obj;
    }

    private class AsyncRequest extends AsyncTask<String, Void, Forecast>{

        private static final String NAME = "name";
        private static final String DESCRIPTION = "description";
        private static final String WEATHER = "weather";
        private static final String MAIN = "main";
        private static final String LIST = "list";
        private static final String TEMP = "temp";
        private static final String ICON = "icon";
        private static final String MAX = "max";
        private static final String MIN = "min";

        @Override
        protected Forecast doInBackground(String... strings) {

            JsonObject current = getWeatherJson(CURRENT_WEATHER_URL, strings[0]);
            JsonObject weekForecast = getWeatherJson(WEEK_FORECAST_URL, strings[0]);

            String cityId;
            String cityName;
            String description;
            Integer currentWeather;
            String iconId;
            List<Integer> temp_max_forecast;
            List<Integer> temp_min_forecast;

            cityId = strings[0];
            cityName = current.get(NAME).getAsString();
            description = current.getAsJsonArray(WEATHER).get(0).getAsJsonObject().get(DESCRIPTION).getAsString();
            //capitalizing first letter in description
            description = description.substring(0,1).toUpperCase() + description.substring(1);
            currentWeather = current.get(MAIN).getAsJsonObject().get(TEMP).getAsInt();
            iconId = current.getAsJsonArray(WEATHER).get(0).getAsJsonObject().get(ICON).getAsString();

            temp_max_forecast = new ArrayList<>(6);
            temp_min_forecast = new ArrayList<>(6);
            for(int i=1; i<7; i++){
                temp_max_forecast.add(weekForecast.getAsJsonArray(LIST).get(i).getAsJsonObject().get(TEMP).getAsJsonObject().get(MAX).getAsInt());
                temp_min_forecast.add(weekForecast.getAsJsonArray(LIST).get(i).getAsJsonObject().get(TEMP).getAsJsonObject().get(MIN).getAsInt());
            }

            return new Forecast(cityId, cityName, description, currentWeather, temp_max_forecast, temp_min_forecast, iconId);
        }
    }
}
