package com.example.parmonig.weather.view;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.parmonig.weather.R;
import com.example.parmonig.weather.model.Forecast;


import java.util.List;

import it.sephiroth.android.library.picasso.Picasso;

/**
 * Created by parmonig on 9/4/17.
 */

public class MyPagerAdapter extends PagerAdapter{

    private List<Forecast> forecasts;
    private Context context;
    private static final String CELSIUS = "\u00b0";
    private static final String BASE_URL = "http://openweathermap.org/img/w/";
    private static final String IMAGE_FORMAT = ".png";

    public MyPagerAdapter(List<Forecast> f, Context c){
        forecasts = f;
        context = c;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    public Object instantiateItem(ViewGroup container, int position){
        View v = LayoutInflater.from(container.getContext()).inflate(R.layout.viewpager_item, container, false);
        TextView cityName = (TextView)v.findViewById(R.id.cityName);
        TextView currentWeather = (TextView)v.findViewById(R.id.currentWeather);
        TextView description = (TextView)v.findViewById(R.id.description);


        cityName.setText(forecasts.get(position).getCityName());
        String weather = forecasts.get(position).getcurrentWeather()+CELSIUS;
        currentWeather.setText(weather);
        description.setText(forecasts.get(position).getDescription());

        ImageView weatherIcon = (ImageView)v.findViewById(R.id.weatherIcon);
        String url = BASE_URL + forecasts.get(position).getIconID() + IMAGE_FORMAT;
        Picasso.with(context).load(url).into(weatherIcon);

        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(forecasts.get(position).getTemp_max_forecast(), forecasts.get(position).getTemp_min_forecast());
        recyclerView.setAdapter(adapter);
        container.addView(v);
        return v;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

}
