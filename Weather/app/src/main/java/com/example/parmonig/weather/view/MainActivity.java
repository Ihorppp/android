package com.example.parmonig.weather.view;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.example.parmonig.weather.R;
import com.example.parmonig.weather.controller.Saver;
import com.example.parmonig.weather.model.Forecast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static boolean firstRun = true;
    private Saver saver = new Saver();
    private Map<String, Integer> pictures;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        //as app reads all objects from file "cities" at first launch we are writing Kiev forecast to "FORECAST"
        if (firstRun) {
//            saver.writeForecastToSharedPrefOnLaunch(this.getApplicationContext());
            saver.writeForecastToAppStorageOnLaunch(this.getApplicationContext());
        }
        firstRun = false;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Forecast> forecasts = new ArrayList<>();
        //reading Forecast objects from file "FORECAST"
        forecasts = saver.readForecastFromAppStorage(this);
//        forecasts = saver.readForecastFromSharedPref(this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);//declared final to use with new Runnable
        MyPagerAdapter adapter = new MyPagerAdapter(forecasts, this);
        viewPager.setAdapter(adapter);

        final ConstraintLayout c = (ConstraintLayout)findViewById(R.id.mainConstraintLayout);
        pictures = getPictures();
        //changing background image dynamically
        final ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                List<Forecast> forecasts = new ArrayList<>();
                forecasts = saver.readForecastFromAppStorage(viewPager.getContext());
//                forecasts = saver.readForecastFromSharedPref(viewPager.getContext());

                for (String key: pictures.keySet()) {
                    if(forecasts.get(position).getIconID().contains(key)){
                        c.setBackgroundResource(pictures.get(key));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
        viewPager.addOnPageChangeListener(pageChangeListener);
        //to invoke page listener on first run(load background image)
        viewPager.post(new Runnable()
        {
            @Override
            public void run()
            {
                pageChangeListener .onPageSelected(viewPager.getCurrentItem());
            }
        });


        //getting number of pressed city from listview(ListOfCitiesActivity)
        //and setting currentItem in viewPager to this city
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            int pressedPageNumber = (int)extras.get("PAGE NUMBER");
            viewPager.setCurrentItem(pressedPageNumber);
        }//maybe it could be done without restarting activity?

        ImageButton showListOfCities = (ImageButton) findViewById(R.id.show_list_of_cities);
        showListOfCities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ListOfCitiesActivity.class);
                view.getContext().startActivity(intent);
                finish();
            }
        });

        ImageButton addCity = (ImageButton) findViewById(R.id.add_city);
        addCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), AddActivity.class);
                view.getContext().startActivity(intent);
                finish();
            }
        });

    }

    private Map<String, Integer> getPictures(){
        Map<String, Integer> pictures =  new HashMap<>();
        pictures.put("01", R.drawable.sun);
        pictures.put("02", R.drawable.dark_clouds);
        pictures.put("03", R.drawable.dark_clouds);
        pictures.put("04", R.drawable.dark_clouds);
        pictures.put("09", R.drawable.rain);
        pictures.put("10", R.drawable.blue_rain);
        pictures.put("11", R.drawable.thunder);
        pictures.put("13", R.drawable.mist_mountain);
        pictures.put("50", R.drawable.mist_and_bird);
        return pictures;
    }
}
