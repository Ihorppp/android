package com.example.parmonig.weather.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.parmonig.weather.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by parmonig on 9/5/17.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

    private List<Integer> temp_max_forecast;
    private List<Integer> temp_min_forecast;

    public MyRecyclerAdapter(List<Integer> temp_max_forecast, List<Integer> temp_min_forecast){
        this.temp_max_forecast = temp_max_forecast;
        this.temp_min_forecast = temp_min_forecast;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        List<String> week = getDays();
        holder.dayOfWeek.setText(week.get(position));
        holder.max_temp.setText(temp_max_forecast.get(position)+"\u00b0");
        holder.min_temp.setText(temp_min_forecast.get(position)+"\u00b0");
    }

    @Override
    public int getItemCount() {
        return temp_max_forecast.size();
    }

    //fabulous method to get array containing 6 dayNames of week from tomorrow
    private List<String> getDays(){
        List<String> week = new ArrayList<>();
        String[] days = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
        for(int i = day; i<day+6; i++){
            week.add(days[i]);
        }
        return week;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView dayOfWeek;
        private TextView max_temp;
        private TextView min_temp;

        public ViewHolder(View v){
            super(v);
            dayOfWeek = (TextView) v.findViewById(R.id.day_of_week);
            max_temp = (TextView) v.findViewById(R.id.max_temp);
            min_temp = (TextView) v.findViewById(R.id.min_temp);
        }
    }
}
