package com.example.parmonig.weather.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.parmonig.weather.R;
import com.example.parmonig.weather.controller.Saver;
import com.example.parmonig.weather.model.Forecast;

import java.util.ArrayList;
import java.util.List;

public class AddActivity extends AppCompatActivity {

    SharedPreferences sPref;
    Saver saver = new Saver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.add_activity);

        final String [] cityCodes = getResources().getStringArray(R.array.city_codes);//strings.xml containing array of all city_codes
        final String [] cityNames = getResources().getStringArray(R.array.city_names);
        final ListView listview = (ListView) findViewById(R.id.add_items_list);

        //allows checking few checkboxees at once
        listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        //all Forecast objects read from file "FORECAST"
        List<Forecast> forecasts = saver.readForecastFromAppStorage(this.getApplicationContext());
//        List<Forecast> forecasts = saver.readForecastFromSharedPref(this.getApplicationContext());
        //list of city names
        final List<String> names = new ArrayList<>();//declared final because it's accessed from inner class
        for(Forecast f: forecasts){
            names.add(f.getCityName());
        }

        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_list_item_multiple_choice, cityNames ){
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_multiple_choice, parent, false);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setText(cityNames[position]);
                //all city names read from file must be checked
                if(names.contains(cityNames[position])){
                    ((ListView)parent).setItemChecked(position, true);
                }
                return v;
            }
        };
        listview.setAdapter(adapter);

        Button buttonAdd = (Button)findViewById(R.id.add_items_button);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SparseBooleanArray sparseBooleanArray = listview.getCheckedItemPositions();
                List<String> checkedCitiesIds = new ArrayList<String>();//all checked cities IDs
                for(int i = 0; i<sparseBooleanArray.size(); i++){
                    int key = sparseBooleanArray.keyAt(i);
                    if(sparseBooleanArray.get(key))
                        checkedCitiesIds.add(cityCodes[key]);
                }
                saver.writeForecastToAppStorage(checkedCitiesIds, view.getContext());
//                saver.writeForecastToSharedPref(checkedCitiesIds, view.getContext());

                Intent intent = new Intent(view.getContext(), MainActivity.class);
                view.getContext().startActivity(intent);
                finish();
            }
        });

    }

}
