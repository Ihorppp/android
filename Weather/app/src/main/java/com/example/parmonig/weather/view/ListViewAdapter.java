package com.example.parmonig.weather.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.parmonig.weather.R;
import com.example.parmonig.weather.model.Forecast;

import java.util.List;

import it.sephiroth.android.library.picasso.Picasso;

/**
 * Created by parmonig on 9/29/17.
 */

public class ListViewAdapter extends BaseAdapter {

    private List<Forecast> forecasts;
    private Context context;
    private static final String BASE_URL = "http://openweathermap.org/img/w/";
    private static final String IMAGE_FORMAT = ".png";
    private static final String CELSIUS = "\u00b0";

    public ListViewAdapter(List<Forecast> forecasts, Context context){
        this.forecasts = forecasts;
        this.context = context;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int position) {
        return forecasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup container) {
        View v = LayoutInflater.from(container.getContext()).inflate(R.layout.listview_item, container, false);
        TextView cityName = (TextView)v.findViewById(R.id.listViewItem_cityName);
        TextView currentWeather = (TextView)v.findViewById(R.id.listViewItem_temp);
        ImageView weatherIcon = (ImageView) v.findViewById(R.id.listViewItem_weatherIcon);

        cityName.setText(forecasts.get(position).getCityName());
        String weather = forecasts.get(position).getcurrentWeather()+ CELSIUS;
        currentWeather.setText(weather);
        //request downloading weather icon from url
        String url = BASE_URL + forecasts.get(position).getIconID() + IMAGE_FORMAT;
        Picasso.with(context).load(url).into(weatherIcon);

        return v;
    }
}
