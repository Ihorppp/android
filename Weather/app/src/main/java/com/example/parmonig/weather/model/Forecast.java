package com.example.parmonig.weather.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by parmonig on 9/4/17.
 */

public class Forecast implements Serializable{

    private String cityName;
    private String cityId;
    private String description;
    private Integer currentWeather;
    private String iconId;

    private List<Integer> temp_max_forecast;
    private List<Integer> temp_min_forecast;


    public Forecast(String cityId, String cityName, String description, Integer currentWeather, List<Integer> temp_max_forecast, List<Integer> temp_min_forecast, String iconId){
        this.cityId = cityId;
        this.cityName = cityName;
        this.description = description;
        this.currentWeather = currentWeather;
        this.temp_max_forecast = temp_max_forecast;
        this.temp_min_forecast = temp_min_forecast;
        this.iconId = iconId;
    }

    public String getCityName(){
        return cityName;
    }
    public String getCityId(){
        return cityId;
    }
    public String getDescription(){
        return description;
    }
    public Integer getcurrentWeather(){
        return currentWeather;
    }
    public List<Integer> getTemp_max_forecast(){
        return temp_max_forecast;
    }
    public List<Integer> getTemp_min_forecast(){
        return temp_min_forecast;
    }
    public String getIconID(){
        return iconId;
    }

}



