package com.example.parmonig.weather.view;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.parmonig.weather.R;
import com.example.parmonig.weather.controller.Saver;
import com.example.parmonig.weather.model.Forecast;

import java.util.ArrayList;
import java.util.List;


public class ListOfCitiesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_of_cities_activity);

        ListView listView = (ListView) findViewById(R.id.listView);

        List<Forecast> forecasts = new ArrayList<>();
        forecasts = new Saver().readForecastFromAppStorage(this);

        ConstraintLayout c = (ConstraintLayout)findViewById(R.id.listOfCitiesConstraintLayout);
        c.setBackgroundResource(R.drawable.space);

        ListViewAdapter adapter = new ListViewAdapter(forecasts, this);
        listView.setAdapter(adapter);

        ImageButton back = (ImageButton)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                view.getContext().startActivity(intent);
                finish();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                intent.putExtra("PAGE NUMBER", i);
                view.getContext().startActivity(intent);
                finish();
            }
        });

        ImageButton addCity = (ImageButton) findViewById(R.id.add_city2);
        addCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), AddActivity.class);
                view.getContext().startActivity(intent);
                finish();
            }
        });

    }
}
